const express = require('express');
const app = express();
var mysql = require('mysql');
var bodyparser = require('body-parser');
var bcrypt = require('bcrypt-nodejs');

app.use(bodyparser.json());
var urlencodedparser = bodyparser.urlencoded({ extended: true });
app.use(urlencodedparser);


var connexion = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'bookshop'
});

var logged_user = "";
var message = "";

connexion.connect();

app.set('view engine', 'ejs');

app.get("/", function (req, res) {
    console.log(logged_user);
    console.log(message);
    connexion.query('select * from book;', function (error, results, fields) {
        if (!error) {
            res.render('books', { results, logged_user, message });
            message = "";
        } else {
            console.log(error);
        }
    })
});

app.use("/book/*", function (req, res, next) {
    if (logged_user) {
        next();
    } else {
        res.send("Vous devez être connecté pour accéder à cette page.");
    }
});

app.get("/book/add", function (req, res) {
    res.render('book_add');
})

app.post("/book/create", urlencodedparser, function (req, res) {
    var sql = 'insert into book (title, author, isbn, nb_pages) values (?, ?, ?, ?);';
    var values = [req.body.title, req.body.author, req.body.isbn, req.body.nb_pages];
    sql = mysql.format(sql, values);
    connexion.query(sql, function (error, results, filelds) {
        if (!error) {
            message = "Le livre a été créé avec succès!";
            res.redirect("/");
        } else {
            console.log(error);
            res.send(error);
        }
    })
})

app.post("/book/edit", urlencodedparser, function (req, res) {
    var sql = 'update book set title = ?, author = ?, isbn = ?, nb_pages = ? where id=?;';
    var values = [req.body.title, req.body.author, req.body.isbn, req.body.nb_pages, req.body.id];
    sql = mysql.format(sql, values);
    connexion.query(sql, function (error, results, filelds) {
        if (!error) {
            res.redirect("/book/" + req.body.id);
        } else {
            console.log(error);
            res.send(error);
        }
    })
})

app.get("/book/delete/:book_id", function (req, res) {
    connexion.query("delete from book where id=" + req.params.book_id, function (error, results, fields) {
        if (!error) {
            message = "Le livre a été supprimé avec succès!";
            res.redirect("/");
        } else {
            res.send(error);
        }
    })
})

app.get("/book/update/:book_id", function (req, res) {
    connexion.query('select * from book where id=' + req.params.book_id, function (error, results, fields) {
        if (!error) {
            res.render('book_update', { book: results[0], book_id: req.params.book_id });
        }
    })
});

app.get("/book/:book_id", function (req, res) {
    connexion.query('select * from book where id=' + req.params.book_id, function (error, results, fields) {
        if (!error) {
            res.render('book', { book: results[0] });
        }
    })
});

app.get("/user/create", function (req, res) {
    res.render("user_add");
});

//Créer compte

app.post("/user/add", urlencodedparser, function (req, res) {
    // On vérifie si on a déjà un compte avec ce mdp
    var select = "SELECT name, mail, password FROM user WHERE mail = (?);";
    var mail = [req.body.email];
    select = mysql.format(select, mail);
    connexion.query(select, function (error, results, fields) {
        if (!error && results.length > 0) {
            res.send("Cette adresse mail est déjà utilisée!")
        }
    });
    // On insère le nouveau compte en bdd
    var sql = "INSERT INTO user (name, mail, password) VALUES (?, ?, ?);"
    var values = [req.body.pseudo, req.body.email, bcrypt.hashSync(req.body.password, null, null)];
    sql = mysql.format(sql, values);
    connexion.query(sql, function (error, results, filelds) {
        if (!error) {
            message = "Votre compte a été créé avec succès!";
            res.redirect("/");
        } else {
            console.log(error);
            res.send(error);
        }
    })
});

app.get("/user/login", function (req, res) {
    res.render("user_login");
});

// Fin création compte

//Connexion

app.post("/user/log", urlencodedparser, function (req, res) {
    var sql = "SELECT name, mail, password FROM user WHERE mail = (?);";
    var values = [req.body.email];
    sql = mysql.format(sql, values);
    connexion.query(sql, function (error, results, fields) {
        if (!error) {
            var msg = "Pas de correspondance trouvée en base";
            if (results.length != 1) {
                res.send(msg);
            } else {
                if (bcrypt.compareSync(req.body.password, results[0].password)) {
                    logged_user = {
                        name: results[0].name,
                        email: results[0].mail
                    };
                    message = "Vous êtes bien connecté!";
                    //revoie à la page que l'on veux après connection
                    res.redirect("/");

                } else {
                    res.send(msg);
                }
            }
        } else {
            res.send(error);
        }
    });
});
// Fin connexion
app.listen(3000, function () { console.log("Listening on port 3000"); });
