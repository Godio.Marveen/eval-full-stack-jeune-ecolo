-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: mydb
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Roles`
--

DROP TABLE IF EXISTS `Roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Roles` (
  `idRoles` int(11) NOT NULL,
  `Rolescol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idRoles`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Roles`
--

LOCK TABLES `Roles` WRITE;
/*!40000 ALTER TABLE `Roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `Roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `idUsers` int(11) NOT NULL AUTO_INCREMENT,
  `Pseudo` varchar(255) DEFAULT NULL,
  `Motdepasse` varchar(255) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idUsers`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'oipo','$2a$10$CHHdilIGpwXyjnJUWXgdLeLM0unTKzZwkLa67P3ZA.YnrcWjNQGtO','jcp@gmail.com'),(2,'Batou','$2a$10$xRvmcXikwvzdv3gLi1/6juCU4SFM/Tmi3t0M1RGUQsv3gzI8/n9sy','batou@gmail.com'),(3,'test','$2a$10$zdQAkWVbfq/OsMeOg7/YzuagfXKt1FB3xH.oH1vqa4CZvvdy8uGhC','test@gmail.com'),(4,'testt','$2a$10$qtDsJKmWoI9XlYMmseJawOCWSt/e.xA5ZobkfJclEB/6AaOVG.ZWS','testt@gmail.com'),(5,'toto','$2a$10$vS9Oiry4WbCBcZX6ilgS8eOK2lB8KCE5joLDANauZWbfG/LWmgz76','toto@toto.com'),(6,'bap','$2a$10$beLGjA6OAEvuiG67TTix0OZOWobAE9JlXa6V2sLk2cK7TZNJ1JKKi','bap@gmail.com'),(7,'oui','$2a$10$UQIg9Lb/uKoat/rVt7bPt.CSiIq/yHvGWdf.IpuFmw6a22Z36Vmuu','oui@oui.com');
--
-- Table structure for table `Roles_has_user`
--

DROP TABLE IF EXISTS `Roles_has_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Roles_has_user` (
  `Users_idUsers` int(11) NOT NULL,
  `Roles_idRoles` int(11) NOT NULL,
  PRIMARY KEY (`Roles_idRoles`,`Users_idUsers`),
  KEY `fk_Roles_has_user_Users_idx` (`Users_idUsers`),
  KEY `fk_Roles_has_user_Roles1_idx` (`Roles_idRoles`),
  CONSTRAINT `fk_Roles_has_user_Roles1` FOREIGN KEY (`Roles_idRoles`) REFERENCES `Roles` (`idRoles`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Roles_has_user_Users` FOREIGN KEY (`Users_idUsers`) REFERENCES `Users` (`idUsers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Roles_has_user`
--

LOCK TABLES `Roles_has_user` WRITE;
/*!40000 ALTER TABLE `Roles_has_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `Roles_has_user` ENABLE KEYS */;
UNLOCK TABLES;

/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-16 13:51:51
