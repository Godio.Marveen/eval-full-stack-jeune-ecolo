const express = require("express");
const mysql = require("mysql");
const bodyParser = require('body-parser');
const app = express();
/*const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;*/
const bcrypt = require('bcrypt-nodejs');
const cookieParser = require('cookie-parser');

//récup valeur input formu
app.use(bodyParser.json());
var urlencodedparser = bodyParser.urlencoded({ extended: false });
app.use(urlencodedparser);
app.use(cookieParser());

//utiliser ejs
app.set("views", "./views");
app.set('view engine', 'ejs');

/*Création des chemins*/

app.get("/", function (req, res) {
    res.render('Acceuil');
});

app.get("/connexion", function (req, res) {
    res.render('connexion');
});

app.get("/menu", function (req, res) {
    res.render('menu');
});

app.get("/profil", function (req, res) {
    res.render('profil');
});

app.get("/defis", function (req, res) {
    res.render('defis');
});

app.get("/evenement", function (req, res) {
    res.render('evenement');
});

app.get("/npmdv", function (req, res) {
    res.render('npmdv');
});

app.get("/pltc", function (req, res) {
    res.render('pltc');
});

app.get("/tsd", function (req, res) {
    res.render('tsd');
});

app.get("/fdlr", function (req, res) {
    res.render('fdlr');
});

app.get("/event", function (req, res) {
    res.render('event');
});

app.get("/formins", function (req, res) {
    res.render('formins');
});
/*Fin des chemins*/

//La base de donnée/////////////////////////////////////////////////////////////////////////////////////

//création de la connexion à la base de donnée

const db = mysql.createConnection({
    host: 'db4free.net',
    user: 'marveen',
    password: 'MrBlackstar1869',
    database: 'evalfullstack'
});

//Connexion base de donnée

db.connect(function (err) {
    if (err) throw err;
    console.log("mysql connecté");
});

//Créer son compte

app.post("/ajoutfiche", urlencodedparser, function (req, res) {
    // On vérifie si on a déjà un compte avec ce mdp
    var select = "SELECT Pseudo, Motdepasse, Email FROM Users WHERE Email = (?);";
    var mail = [req.body.Email];
    select = mysql.format(select, mail);
    db.query(select, function (error, results, fields) {
        if (!error && results.length > 0) {
            res.send("Cette adresse mail est déjà utilisée!")
        }
    });
    // On insère le nouveau compte en bdd
    var sql = "INSERT INTO Users (Pseudo, Email, Motdepasse) VALUES (?, ?, ?);"
    var values = [req.body.pseudo, req.body.email, bcrypt.hashSync(req.body.motdepasse, null, null)];
    sql = mysql.format(sql, values);
    db.query(sql, function (error, results, fields) {
        if (!error) {
            message = "Votre compte a été créé avec succès!";
            //après inscrit redirige vers la page:
            res.redirect("/connexion");
        } else {
            console.log(error);
            res.send(error);
        }
    })
});

//Se Connecter

app.post("/formconnect", urlencodedparser, function (req, res) {
    console.log(req.body);
    var sql = "SELECT Pseudo, Motdepasse FROM Users WHERE Pseudo = (?);";
    var values = req.body.pseudo;
    console.log(values);
    sql = mysql.format(sql, values);
    db.query(sql, function (error, results, fields) {
        if (!error) {
            var msg = "Pas de correspondance trouvée en base";
            if (results.length != 1) {
                console.log(results);
                res.send(msg);
            } else {
                if (bcrypt.compareSync(req.body.motdepasse, results[0].Motdepasse)) {
                    logged_user = {
                        Pseudo: results[0].Pseudo,
                    };
                    message = "Vous êtes bien connecté!";
                    //revoie à la page que l'on veux après connection
                    res.redirect("/menu");

                } else {
                    res.send(msg);
                }
            }
        } else {
            res.send(error);
        }
    });
});
// Fin se Connecter


app.use(express.static("public"));
/*app.use(passport.initialize());
app.use(passport.session());*/
let port = process.env.PORT || 3000;

app.listen(port, function () {
    console.log('le serveur écoute sur le port '+port);
});
